
export const environment = {
  production: true,
  apiUsers: "your api url",
  apiKey: "your api key",
  apiPokemon: 'https://pokeapi.co/api/v2/pokemon?limit=154&offset=0'
 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

