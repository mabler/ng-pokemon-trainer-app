import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { StorageKeys } from 'src/app/enums/storage-keys.enums';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  /**
   * Import user from userService
   * Return user and access user object inside html template
   * handleLogout removes user from storage
   */
  get user(): User | undefined {
    return this.userService.user
  }
  
  constructor(private readonly userService:UserService) { }
  
  handleLogout() : void {  
    
   this.userService.removeUser(StorageKeys.User);

   }

  ngOnInit(): void {
  }

}
