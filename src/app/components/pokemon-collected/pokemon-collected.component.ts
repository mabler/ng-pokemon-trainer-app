import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { CollectedService } from 'src/app/services/pokemon-collected.service';
import { UserService } from 'src/app/services/user.service';
import { LoginFormComponent } from '../login-form/login-form.component';

@Component({
  selector: 'app-pokemon-collected',
  templateUrl: './pokemon-collected.component.html',
  styleUrls: ['./pokemon-collected.component.css']
})
export class PokemonCollectedComponent implements OnInit {
  /**
   * Check if pokemon is collected
   */
  public isCollected: boolean = false

  @Input() pokemonId: string = ""

  constructor(
    private collectedService: CollectedService,
    private readonly userService: UserService
  ) { }

  ngOnInit(): void {
    this.isCollected = this.userService.inCollection(this.pokemonId)    
  }

  handlePokemonClick(): void{
    // add pokemon to collection by id
  
    this.collectedService.addToCollection(this.pokemonId)
    
    .subscribe({
      
      next: (user: User) => {
        this.isCollected = this.userService.inCollection(this.pokemonId)
        
      },
      error: (error: HttpErrorResponse) => {
        console.log(error.message);
        
      }
    })
  }

}
