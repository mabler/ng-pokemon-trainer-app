import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonCollectedComponent } from './pokemon-collected.component';

describe('PokemonCollectedComponent', () => {
  let component: PokemonCollectedComponent;
  let fixture: ComponentFixture<PokemonCollectedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonCollectedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PokemonCollectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
