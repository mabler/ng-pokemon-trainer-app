export interface Pokemon  {
    name:string,
    url: string,
    id: string,
    imgUrl: string,
    isCollected: boolean
}
