import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { CollectedService } from 'src/app/services/pokemon-collected.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {

  constructor(
    private readonly userService: UserService,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly collectedService: CollectedService) { }

    get user(): User | undefined {
      return this.userService.user
    }

    get pokemons(): Pokemon[]{
      if(this.userService.user){
        return this.userService.user.pokemon
      }
      return []
    }

  ngOnInit(): void {
    console.log(this.pokemons)
    
  }

  

}
