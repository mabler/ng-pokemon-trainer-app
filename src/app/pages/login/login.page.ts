import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage  {

  constructor(private readonly router: Router) { }

 
/**
 *  Navigate to login page after successful login
 */
  handleLogin(): void {
    this.router.navigateByUrl("/pokemons");
  }

}
