import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, tap } from "rxjs";
import { environment } from "src/environments/environment";
import { Pokemon } from "../models/pokemon.model";
import { User } from "../models/user.model";
import { PokemonCatalogueService } from "./pokemon-catalogue.service";
import { UserService } from "./user.service";

const { apiKey, apiUsers } = environment

@Injectable({
    providedIn: 'root'
})

export class CollectedService {
    constructor(
        private readonly pokemonService: PokemonCatalogueService,
        private readonly userService:  UserService,
        private http: HttpClient
    ){}

    public addToCollection(pokemonId: string): Observable<User>{
        
        // if there is no user
        if(!this.userService.user){
            throw new Error('There is no user.')
        }

        const user: User = this.userService.user

        const pokemon: Pokemon | undefined = this.pokemonService.findPokemonById(pokemonId)

        // if there is no pokemon
        if(!pokemon){
            throw new Error(`No pokemon with id: ${pokemonId}`)
        }
        // if the pokemon is in users collection, remove it. if not, add it.

        if(this.userService.inCollection(pokemonId)){
            this.userService.removeFromCollection(pokemonId)
            
        }
        else{
            this.userService.addToCollection(pokemon)

        }
         
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-api-key': apiKey
        })

        return this.http.patch<User>(`${apiUsers}/${user.id}`, {
            pokemon: [...user.pokemon]
        },{ headers }
        )
        .pipe(
            tap((updatedUser: User) => {
                this.userService.user = updatedUser             
            })
        )


    }
    
}