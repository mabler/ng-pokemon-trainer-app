import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enums';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.utils';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: User;

 get user(): User | undefined {
    return this._user;
  }
  set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!)
     this._user = user; 
  }
  public removeUser(key: string): void{

    StorageUtil.storageRemove(key); // Delete the sessionStorage with key

    this.router.navigateByUrl("/login") // Navigate back to loginpage

    window.location.reload();

  }
  constructor( private readonly router: Router) { 
    this._user = StorageUtil.storageRead<User>(StorageKeys.User)
  }

  // check if pokemon with the provided id exists in the users collected pokemon.
  public inCollection(pokemonId: string): boolean{
    
    if(this._user){
      //returns false if pokemon is not in collection - if user has the object in collection, returns true
      for(let i = 0; i < this._user.pokemon.length; i++){
        if(this._user.pokemon[i].id === pokemonId){
          return true
        }
      }
    }
    return false
  }

  // remove pokemon from users collection
  public removeFromCollection(pokemonId: string): void {
    
    if(this._user){
      // filter out all pokemons whose id is not equal to the provided id
      console.log(this._user.pokemon);
      for(let i = 0; i < this._user.pokemon.length; i++){
        if(this._user.pokemon[i].id === pokemonId){
          this._user.pokemon.splice(i, 1)
        }  
        
      }

    }
  }

  // add pokemon to users collection
  public addToCollection(pokemon:Pokemon): void {
    
    if(this._user) {
      pokemon.isCollected === true
      this._user.pokemon.push(pokemon)
    }
  }
}

