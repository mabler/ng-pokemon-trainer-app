import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Info } from '../models/pokemon-api.model';
import { finalize, switchMap } from 'rxjs';

const { apiPokemon } = environment;

@Injectable({
  providedIn: 'root',
})
export class PokemonCatalogueService {
  private _pokemons: Pokemon[] = []
  private _error: string = ''
  private _loading: boolean = false

  get pokemons(): Pokemon[] {
    return this._pokemons
  }

  get loading(): boolean {
    return this._loading
  }

  get error(): string {
    return this._error
  }

  constructor(private readonly http: HttpClient) {}

  // method for extracting id
  private getPokemonId(pokemon: Pokemon): string {
    const baseUrl ="https://pokeapi.co/api/v2/pokemon/"
    const pokemonId = pokemon.url.substring(baseUrl.length, pokemon.url.length -1)
    
    return pokemonId
  }

  public getPokemons(): void {
    this._loading = true
    // check if there are pokemons in storage before fetching
    if (sessionStorage.getItem('pokemons') !== null) {
      // parse string from storage
      const pokemonsInStorage = JSON.parse(
        sessionStorage.getItem('pokemons') || ''
      )
      // push each object to array
      pokemonsInStorage.forEach((pokemon: Pokemon) => {
        this._pokemons.push(pokemon)
      })
      // finish method if pokemons are found in storage
      this._loading = false
      return
    }

    this.http
      .get<any>(apiPokemon)
      .pipe(
        finalize(() => {
            this._loading = false
        })
      )
      // subscribe listens for updates
      .subscribe({
        next: (response) => {
          let pokemons = response.results;

          pokemons.forEach((pokemon: Pokemon) => {
            // get id
            pokemon.id = this.getPokemonId(pokemon)
            
            // add imgUrl to object
            pokemon.imgUrl = (`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`)

            // set isCollected to false
            pokemon.isCollected = false

            // push pokemon objects to array
            this._pokemons.push(pokemon)    
            // save pokemons to storage
            sessionStorage.setItem('pokemons', JSON.stringify(pokemons))
          })
        },
        error: (error: HttpErrorResponse) => {
            this._error = error.message
        }
      })
  }

  public findPokemonById(id: string): Pokemon | undefined{
    return this._pokemons.find((pokemon: Pokemon) => pokemon.id === id)
  }

}
