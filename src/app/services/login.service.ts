import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable,  map, switchMap,of,} from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enums';
import {User} from '../models/user.model'
import { StorageUtil } from '../utils/storage.utils';

 

const {apiUsers,apiKey}= environment

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private readonly http: HttpClient,

  ) { }

/**
 * 
 * @param username 
 * @returns if user is undefined create new user
 */
  public login(username:string): Observable<User> {
    return this.checkUsername(username)
    .pipe(
        switchMap((user: User | undefined) => {
          if (user === undefined) {
            return this.createUser(username)
          }
          return of(user)
 
  }),
    )

   
  }
/**
 * 
 * @param username 
 * @returns checks if username exists
 */
private checkUsername(username:string): Observable<User | undefined> {
  return this.http.get<User[]>(`${apiUsers}?username=${username}`)
    .pipe(

      map((response:User[]) =>{
        return response.pop()
      })
    )

}
/**
 * 
 * @param username 
 * @returns creates user
 */
private createUser(username:string): Observable<User> {
  const user = {
    username: username,
    pokemon: []
  }

const headers = new HttpHeaders({
  "Content-Type": "application/json",
  "x-api-key": apiKey
})

return this.http.post<User>(apiUsers,user, {
  headers
 
})


}
}