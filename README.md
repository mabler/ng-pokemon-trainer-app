# Pokemon Trainer App

An Angular app where a user can log in with a pokemon trainer name and save pokemons. If no user with the provided trainer name exists on the API, a new user is created.
The user is redirected to the pokemon catalogue on login. We fetch 154 pokemons from the [PokeApi](https://pokeapi.co/), which are then saved to sessionStorage. Via the navbar the logged in user can navigate to their trainer-page and log out. On the trainer page the user can see all their collected pokemons and remove them from the collection.

Link to live app: [Pokemon-trainer](https://maltinmartinapoke.herokuapp.com/)

[Pokemon icons created by Darius Dan - Flaticon](https://www.flaticon.com/free-icons/pokemon)

## Install
Clone the app
```
git clone https://gitlab.com/mabler/ng-pokemon-trainer-app
```
cd into project root
```
cd pokemon-trainer-app
```
Install dependencies
```
npm install
```
Run server
```
ng serve
```

## Usage

Add URL to API and API-key to environment.ts
```
apiKey: <your-api-key>
apiUsers: <your-api-url>
```

## Contributors
[Maltin Coku](@maltin1234)

[Martina Blixt Eriksson](@MartinaBlixtEriksson)
